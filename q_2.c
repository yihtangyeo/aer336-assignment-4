// Code written by Yih Tang Yeo 999295132
// AER336 Assignment 4
// Question 2
// This code is exactly the same as q_1.c, but only the matrix is changed since a different formula is given

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define PI 3.1415926535

float*  array_nonlinear;
float** array_jacobian;
float** array_modified;                                     // for cramer's rule
int     N = 3;                                              // size of array N x N
float*  array_delta_x;                                      // delta x = x(k+1) - x(k)
float*  array_x;                                            // current estimate of x
float*  array_x_previous;                                   // previous estimate of x
// note that the value of k is not known as the converge criteria is set up in that way.
// To avoid performance issue related to dynamic memory allocation, only the previous
// values are stored, but past results will be exported as a text file instead, so that
// the values at each k can be imported into MATLAB for plotting.

float find_determinant(float**);

int main(){

    // memory allocation for array;
    array_nonlinear     = malloc(sizeof(float)*N);
    array_delta_x       = malloc(sizeof(float)*N);
    array_jacobian      = malloc(sizeof(float*)*N);
    array_modified      = malloc(sizeof(float*)*N);
    array_x             = malloc(sizeof(float)*N);
    array_x_previous    = malloc(sizeof(float)*N);

    int i;
    for (i = 0; i < N; i++){
        array_jacobian[i] = malloc(sizeof(float)*N);
        array_modified[i] = malloc(sizeof(float)*N);
    }

    // assigning values (initial guess)
    array_x_previous[0] = array_x[0] = 1.0f;
    array_x_previous[1] = array_x[1] = 1.0f;
    array_x_previous[2] = array_x[2] = -1.0f;

    // open file to write externally
    FILE *fp;
    char* filename = "q_2_output.txt";
    fp = fopen(filename, "w");
    // error handling
    if (fp == NULL){
        printf("ERROR: Can't create file.");
        exit(1);
    }
    // print header information
    fprintf(fp,"%13s \t %13s \t %13s \t %13s \n", "Iter", "x", "y", "z");


    int k = 0;                                          // number of iteration count
    int m = 0, p = 0;                                   // for looping index
    float error_largest;                                // for convergence criteria
    do {

        k++;                                            // increment counter

        // set up F matrix
        // negative is to move it to the RHS
        array_nonlinear[0] = -(3*array_x[0] -cos(array_x[1]*array_x[2]) - 0.5);
        array_nonlinear[1] = -(array_x[0]*array_x[0] - 625*(array_x[1]*array_x[1]) - 0.25);
        array_nonlinear[2] = -(exp(-array_x[0]*array_x[1]) + 20*array_x[2] + (10*PI-3)/3);

        // set up Jacobian matrix
        // note that the notation in this case [column][row]
        array_jacobian[0][0] = 3;                                       // df1/dx1
        array_jacobian[1][0] = array_x[2]*sin(array_x[1]*array_x[2]);   // df1/dx2
        array_jacobian[2][0] = array_x[1]*sin(array_x[1]*array_x[2]);   // df1/dx3
        array_jacobian[0][1] = 2*array_x[0];                            // df2/dx1
        array_jacobian[1][1] = -625*2*(array_x[1]);                     // df2/dx2
        array_jacobian[2][1] = 0;                                       // df2/dx3
        array_jacobian[0][2] = -array_x[1]*exp(-array_x[0]*array_x[1]); // df3/dx1
        array_jacobian[1][2] = -array_x[0]*exp(-array_x[0]*array_x[1]); // df3/dx2
        array_jacobian[2][2] = 20;                                      // df3/dx3

        printf("\n%7.5f %7.5f %7.5f \n", array_jacobian[0][0], array_jacobian[1][0], array_jacobian[2][0]);
        printf("%7.5f %7.5f %7.5f \n", array_jacobian[0][1], array_jacobian[1][1], array_jacobian[2][1]);
        printf("%7.5f %7.5f %7.5f \n", array_jacobian[0][2], array_jacobian[1][2], array_jacobian[2][2]);
        printf("Jacobian: %f\n", find_determinant(array_jacobian));

        // solve for delta_x using Cramer's Rule
        // solve system F + J delta_x = 0 <=> J delta_x = -F
        // note that on the right hand side we got -F matrix
        for (m = 0; m < N; m++){
            for (p = 0; p < N; p++){
                if (p != m)
                    array_modified[p] = array_jacobian[p];
                else
                    array_modified[p] = array_nonlinear;
                // note that the notation in this case [column][row]
            }

            array_delta_x[m] = find_determinant(array_modified)/find_determinant(array_jacobian);
            array_x[m] = array_delta_x[m] + array_x_previous[m];
        }

        // print results to file
        fprintf(fp,"%13d \t %13.10f \t %13.10f \t %13.10f \n", k, array_x[0], array_x[1], array_x[2]);

        // Convergence criteria: compare to get the largest error
        error_largest = 0.0f;
        for (m = 0; m < N; m++){
            if (fabs(array_delta_x[m])> error_largest){
                error_largest = fabs(array_delta_x[m]);
            }
        }

        // store it as previous value
        array_x_previous[0] = array_x[0];
        array_x_previous[1] = array_x[1];
        array_x_previous[2] = array_x[2];

    }while (error_largest > 10e-6);     // repeat until largest error is less than 10e-6

    // print results
    printf("Solution converged after %d iterations.\n", k);
    printf("Final values \n\tx = %.10f, \n\ty = %.10f, \n\tz = %.10f\n", array_x[0], array_x[1], array_x[2]);

    fclose(fp);
    return 0;
}


float find_determinant(float** matrix){
    // equation for finding a determinant for a 3 by 3 system
    float determinant = matrix[0][0]*(matrix[1][1]*matrix[2][2]-matrix[1][2]*matrix[2][1]) -
                        matrix[0][1]*(matrix[1][0]*matrix[2][2]-matrix[1][2]*matrix[2][0]) +
                        matrix[0][2]*(matrix[1][0]*matrix[2][1]-matrix[1][1]*matrix[2][0]);
    return determinant;
}
