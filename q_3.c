// Code written by Yih Tang Yeo 999295132
// AER336 Assignment 4
// Question 3

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define PI 3.1415926535

float*  array_differential;
float** array_hessian;
float** array_modified;                                     // for cramer's rule
int     N = 3;                                              // size of array N x N
float*  array_delta_x;                                      // delta x, delta y, delta z
float*  array_x;                                            // current estimate of x, y, z
float*  array_x_previous;                                   // previous estimate of x, y, z
float   value_f;                                            // maximum value of f
// note that the value of k is not known as the converge criteria is set up in that way.
// To avoid performance issue related to dynamic memory allocation, only the previous
// values are stored, but past results will be exported as a text file instead, so that
// the values at each k can be imported into MATLAB for plotting.

float find_determinant(float**);

int main(){

    // memory allocation for array;
    array_differential  = malloc(sizeof(float)*N);
    array_delta_x       = malloc(sizeof(float)*N);                // delta x, delta y, delta z
    array_hessian       = malloc(sizeof(float*)*N);               // d2f/dx2
    array_modified      = malloc(sizeof(float*)*N);              // for Crammer's rule numertaor
    array_x             = malloc(sizeof(float)*N);                      // x, y, z
    array_x_previous    = malloc(sizeof(float)*N);
    int i;
    for (i = 0; i < N; i++){
        array_hessian[i]  = malloc(sizeof(float)*N);
        array_modified[i] = malloc(sizeof(float)*N);
    }

    // assigning values (initial guess)
    array_x_previous[0] = array_x[0] = 0.0f;
    array_x_previous[1] = array_x[1] = 0.0f;
    array_x_previous[2] = array_x[2] = 0.0f;

    // open file to write externally
    FILE *fp;
    char* filename = "q_3_output.txt";
    fp = fopen(filename, "w");
    // error handling
    if (fp == NULL){
        printf("ERROR: Can't create file.");
        exit(1);
    }
    // print header information
    fprintf(fp,"%13s \t %13s \t %13s \t %13s \t %13s \t %13s \t %13s \t %13s \n", "Iter", "grad_x", "grad_y", "grad_z", "x", "y", "z", "f");

    int k = 0;                                          // number of iteration count
    int m = 0, p = 0;                                   // for looping index
    float error_largest;                                // for convergence criteria
    do {
        // replace matrix with x,y,z to make equation typing more readable
        float x = array_x[0];
        float y = array_x[1];
        float z = array_x[2];

        k++;                                            // increment counter
        fprintf(fp,"%13d \t ", k);                      // write to output

        // set up F matrix
        // negative is to move it to the RHS
        // df/dx = 2x-2y+2+2yz exp(xyz)
        array_differential[0] = -(2*x-2*y+2+2*y*z*exp(x*y*z));
        // df/dy = 4y-2x-2.5+2xz exp(xyz)
        array_differential[1] = -(4*y-2*x-2.5+2*x*z*exp(x*y*z));
        // df/dz = 2z-1+2xy exp(xyz)
        array_differential[2] = -(2*z-1+2*x*y*exp(x*y*z));

        fprintf(fp,"%13.10f \t %13.10f \t %13.10f \t ", array_differential[0], array_differential[1], array_differential[2]);

        // set up Jacobian matrix
        // note that the notation in this case [column][row]
        array_hessian[0][0] = 2 + 2*y*y*z*z*exp(x*y*z);                     // d2f/dx2  = 2 + 2y^2 z^2 exp(xyz)
        array_hessian[1][0] = -2 + 2*z*exp(x*y*z) + 2*x*y*z*z*exp(x*y*z);   // d2f/dxdy = -2 +2z exp(xyz) + 2xyz^2 exp(xyz)
        array_hessian[2][0] = 2*y*exp(x*y*z) + 2*x*y*y*z*exp(x*y*z);        // d2f/dxdz = 2y exp(xyz) + 2xy^2z exp(xyz)
        array_hessian[0][1] = -2 + 2*z*exp(x*y*z) + 2*x*y*z*z*exp(x*y*z);   // d2f/dydx = -2 +2z exp(xyz) + 2 xyz^2 exp(xyz)
        array_hessian[1][1] = 4 + 2*x*x*z*z*exp(x*y*z);                     // d2f/dy2  = 4+2x^2z^2 exp(xyz)
        array_hessian[2][1] = 2*x*exp(x*y*z) + 2*x*x*y*z*exp(x*y*z);         // d2f/dydz = 2x exp(xyz)+2x^2yz exp(xyz)
        array_hessian[0][2] = 2*y*exp(x*y*z) + 2*x*y*y*z*exp(x*y*z);        // d2f/dzdx = 2y exp(xyz) + 2xy^2z exp(xyz)
        array_hessian[1][2] = 2*x*exp(x*y*z) + 2*x*x*y*z*exp(x*y*z);         // d2f/dzdy = 2x exp(xyz)+2x^2yz exp(xyz)
        array_hessian[2][2] = 2 + 2*x*x*y*y*exp(x*y*z);                     // d2f/dz2  = 2+2x^2y^2 exp(xyz)

        // solve for delta_x using Cramer's Rule
        // solve system F + J delta_x = 0 <=> J delta_x = -F
        // note that on the right hand side we got -F matrix
        for (m = 0; m < N; m++){
            for (p = 0; p < N; p++){
                if (p != m)
                    array_modified[p] = array_hessian[p];
                else
                    array_modified[p] = array_differential;
                // note that the notation in this case [column][row]
            }
            array_delta_x[m] = find_determinant(array_modified)/find_determinant(array_hessian);
            array_x[m] = array_delta_x[m] + array_x_previous[m];
        }

        // find corresponding F value
        value_f = x*x+2*y*y+z*z-2*x*y+2*x-2.5*y-z+2*exp(x*y*z);

        // print results to file
        fprintf(fp,"%13.10f \t %13.10f \t %13.10f \t %13.10f \n", array_x[0], array_x[1], array_x[2], value_f);

        // Convergence criteria: compare to get the largest error
        error_largest = 0.0f;
        for (m = 0; m < N; m++){
            if (fabs(array_delta_x[m])> error_largest){
                error_largest = fabs(array_delta_x[m]);
            }
        }

        // store it as previous value
        array_x_previous[0] = array_x[0];
        array_x_previous[1] = array_x[1];
        array_x_previous[2] = array_x[2];

    }while (error_largest > 10e-6);     // repeat until largest error is less than 10e-6

    // print results
    printf("Solution converged after %d iterations.\n", k);
    printf("Minimum value of f = %.10f \n   when x = %.10f, \n\ty = %.10f, \n\tz = %.10f\n", value_f, array_x[0], array_x[1], array_x[2]);

    // confirmation it's the minimum
    float subdeterminant_1 = array_hessian[1][1];
    float subdeterminant_2 = array_hessian[0][0]*array_hessian[1][1]- array_hessian[0][1]*array_hessian[1][0];
    float subdeterminant_3 = find_determinant(array_hessian);
    printf("Proof of minimum: \n\tSubdeterminant 1 = %f\n\tSubdeterminant 2 = %f\n\tSubdeterminant 3 = %f\n", subdeterminant_1, subdeterminant_2, subdeterminant_3);

    fclose(fp);
    return 0;
}


float find_determinant(float** matrix){
    // equation for finding a determinant for a 3 by 3 system
    float determinant = matrix[0][0]*(matrix[1][1]*matrix[2][2]-matrix[1][2]*matrix[2][1]) -
                        matrix[0][1]*(matrix[1][0]*matrix[2][2]-matrix[1][2]*matrix[2][0]) +
                        matrix[0][2]*(matrix[1][0]*matrix[2][1]-matrix[1][1]*matrix[2][0]);
    return determinant;
}
